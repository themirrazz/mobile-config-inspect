var MblCfg={
    read:function(xmlraw) {
        var data={
            displayName: "",
            description: "",
            identifier: "",
            uuid: "",
            organization: "",
            payloads: [],
            durationUntilRemoved:Infinity
        }
        var Payload=class Payload {
            constructor() {}
            uuid="";
            type="com.apple.unknown.managed";
            version=1
            identifier="";
            description="";
            payloadName="";
        }
        var TemplatePayload=class TemplatePayload extends Payload {
            constructor() {super()}
        }
        var ProxyConfig=class ProxyConfig {
            constructor(){}
            type="automatic"
            host=""
            port=0
            username=""
            password=""
            pacURL=""
        }
        var WifiPayload=class WifiPayload extends Payload {
            constructor() {super()}
            ssid=""
            autoJoin=false
            hidden=false
            hotspot=false
            encryptionType="none"
            roamingEnabled=false
            proxy=new ProxyConfig()
            username=null
            password=null
            bypassCaptivePortal=false
        }
        var AirplayPayload=class AirplayPayload extends Payload {
            constructor() {super()}
            deviceWhitelist=[];
            devicePasswords=[];
        }
        var WebClipPayload=class WebClipPayload extends Payload {
            constructor() {super()}
            icon=new FileData(btoa('hello, world'));
            clipLabel="";
            clipURL="https://example.com/";
            clipCanBeRemoved=true;
            fullscreen=false;
        }
        var EmailConfigPayload=class EmailConfigPayload extends Payload {
            constructor() {super()}
            accountDescription="";
            accountName="";
            connectionType="imap";
            serverIncomingAuth="EmailAuthPassword";
            serverOutgoingAuth="EmailAuthPassword";
            emailAddress=""
            serverIncomingHost=""
            serverIncomingPort=0
            serverIncomingSSL=true
            serverIncomingUsername=""
            serverIncomingPassword=""
            serverOutgoingHost=""
            serverOutgoingPort=0
            serverOutgoingSSL=true
            serverOutgoingUsername=""
            serverOutgoingPassword=""
            preventMoving=false
            preventSendingInShareSheet=false
            allowMailDrop=true
            smimeeEnabled=false
        }
        var FileData = function (url) {
            this.getAsBase64=function() {
                return url
            };
            this.getAsUint8Array=async function () {
                var f=await fetch(`data:application/octet-stream;base64,${url}`);
                return new Uint8Array(
                    await f.arrayBuffer()
                )
            }
            this.getAsBlob=async function () {
                var f=await fetch(`data:application/octet-stream;base64,${url}`);
                return await f.blob()
            }
            this.getAsBlobURL=async function () {
                var f=await fetch(`data:application/octet-stream;base64,${url}`);
                return URL.createObjectURL(await f.blob())
            }
        }
        /**
         * @param e{Element}
         * @param t{string}
         */
        function getType(e,t) {
            if(t==='string') {
                return e.textContent;
            } else if(t==='integer'||t==='decimal') {
                return Number(e.textContent)
            } else if(t==='true') {
                return true
            } else if (t==='false') {
                return false
            } else if(t==='dict') {
                var dc=e.children;
                var ob={};
                for(var i=0;i<Math.floor(dc.length/2);i++) {
                    ob[dc[i*2].textContent]=getType(
                        dc[(i*2)+1],
                        dc[(i*2)+1].nodeName.toLowerCase()
                    );
                }
                return ob
            } else if(t==='array') {
                var a=[];
                var ac=e.children;
                for(var i=0;i<ac.length;i++) {
                    a.push(
                        getType(ac[i],
                        ac[i].nodeName.toLowerCase())
                    )
                }
                return a
            } else if(t==='data') {
                return new FileData(e.textContent)
            } else {
                return null
            }
        }
        var xml=xmlraw.slice(
            xmlraw.indexOf("<!DOCTYPE plist PUBLIC"),
            xmlraw.lastIndexOf("</plist>")+8
        );
        var parser=new DOMParser();
        var dom=parser.parseFromString(xml,'application/xml');
        if(dom.querySelector('parseerror')) {
            throw new Error(dom.querySelector('parseerror div').textContent);
        }
        if(dom.documentElement!=dom.querySelector('plist')) {
            throw new Error("invalid plist tree")
        }
        var dict=dom.documentElement.children[0];
        if(!dict) {
            throw new Error("missing main dictionary")
        }
        if(!dict.nodeName.toLowerCase()==="dict") {
            throw new Error('first child must be main dictionary')
        }
        var xmr=getType(dict,'dict');
        /**
         * Prevent one payload in object format from destroying the entire parse
         */
        if(!(xmr.PayloadContent instanceof Array)) {
            xmr.PayloadContent=[xmr.PayloadContent];
        };
        data.description=xmr.PayloadDescription;
        data.displayName=xmr.PayloadDisplayName;
        data.identifier=xmr.PayloadIdentifier;
        data.organization=xmr.PayloadOrganization;
        data.uuid=xmr.PayloadUUID;
        xmr.PayloadContent.forEach(details=>{
            var type=details.PayloadType;
            var payload;
            if(type==='com.apple.mail.managed') {
                payload=new EmailConfigPayload();
                payload.serverIncomingAuth=details.IncomingMailServerAuthentication;
                payload.serverOutgoingAuth=details.OutgoingMailServerAuthentication;
                payload.serverIncomingUsername=details.IncomingMailServerUsername;
                payload.serverOutgoingUsername=details.OutgoingMailServerUsername;
                payload.serverOutgoingHost=details.OutgoingMailServerHostName;
                payload.serverIncomingHost=details.IncomingMailServerHostName;
                payload.serverIncomingPort=details.IncomingMailServerPortNumber;
                payload.serverOutgoingPort=details.OutgoingMailServerPortNumber;
                payload.serverIncomingSSL=details.IncomingMailServerUseSSL;
                payload.serverOutgoingSSL=details.OutgoingMailServerUseSSL;
                payload.smimeeEnabled=details.SMIEEEnabled;
                payload.allowMailDrop=details.allowMailDrop;
                payload.serverIncomingPassword=details.IncomingPassword;
                payload.serverOutgoingPassword=details.OutgoingPasswordSameAsIncomingPassword?details.IncomingPassword:details.OutgoingPassword;
                payload.preventMoving=details.PreventMove;
                payload.preventSendingInShareSheet=details.PreventAppSheet;
                payload.accountDescription=details.EmailAccountDescription;
                payload.accountName=details.EmailAccountName;
            } else if(type==='com.apple.webClip.managed') {
                payload=new WebClipPayload();
                payload.clipURL=details.URL;
                payload.name=details.Label;
                payload.icon=details.Icon;
                payload.fullscreen=details.Fullscreen
            } else if(type==='com.apple.airplay'||type==='com.apple.airplay.managed') {
                payload=new AirplayPayload();
                details.Whitelist.forEach(dev=>{
                    payload.deviceWhitelist.push(dev)
                });
                details.Passwords.forEach(o=>{
                    payload.devicePasswords.push({
                        deviceID:o.DeviceID,
                        deviceName:o.DeviceName,
                        devicePasscode:o.Password
                    });
                });
            } else if(type==='com.apple.wifi.managed') {
                payload=new WifiPayload();
                payload.ssid=details.SSID_STR;
                payload.encryptionType=details.EncryptionType;
                payload.bypassCaptivePortal=details.CaptiveBypass;
                payload.hotspot=details.IsHotspot;
                payload.proxy.host=details.ProxyServer;
                payload.proxy.type=details.ProxyType;
                payload.proxy.port=details.ProxyServerPort;
                payload.proxy.username=details.ProxyUsername;
                payload.proxy.password=details.ProxyPassword;
                payload.proxy.pacURL=details.ProxyPACURL;
                payload.hidden=details.HIDDEN_NETWORK;
                payload.autoJoin=details.AutoJoin;
            } else {
                return;
            }
            payload.uuid=details.PayloadUUID;
            payload.payloadName=details.PayloadDisplayName;
            payload.description=details.PayloadDescription||null;
            payload.identifier=details.PayloadIdentifier;
            payload.type=details.PayloadType;
            data.payloads.push(payload);
        })
        data.durationUntilRemoved=Number(xmr.DurationUntilRemoval)||Infinity
        return data;
    }
}
